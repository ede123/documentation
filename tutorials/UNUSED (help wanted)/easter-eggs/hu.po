# Translators:
# Gyuris Gellért <bubu@ujevangelizacio.hu>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Easter Eggs\n"
"POT-Creation-Date: 2009-05-24 11:40+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Gyuris Gellért <bubu@ujevangelizacio.hu>, 2018\n"
"Language-Team: Hungarian (https://www.transifex.com/fsf-hu/teams/77907/hu/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hu\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: easter-eggs-f13.svg44(format) easter-eggs-f12.svg44(format)
#: easter-eggs-f11.svg44(format) easter-eggs-f10.svg44(format)
#: easter-eggs-f09.svg44(format) easter-eggs-f08.svg44(format)
#: easter-eggs-f07.svg44(format) easter-eggs-f06.svg44(format)
#: easter-eggs-f05.svg44(format) easter-eggs-f04.svg44(format)
#: easter-eggs-f03.svg44(format) easter-eggs-f02.svg44(format)
#: easter-eggs-f01.svg44(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml24(None)
msgid "@@image: 'easter-eggs-f01.svg'; md5=533640baaf821717dbd4fff4d7af1386"
msgstr "@@image: 'easter-eggs-f01.svg'; md5=533640baaf821717dbd4fff4d7af1386"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml35(None)
msgid "@@image: 'easter-eggs-f02.svg'; md5=8b0ba516bd6144f447792430930e08c9"
msgstr "@@image: 'easter-eggs-f02.svg'; md5=8b0ba516bd6144f447792430930e08c9"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml46(None)
msgid "@@image: 'easter-eggs-f03.svg'; md5=5a38534f4d800140ecd1b435e43cc30e"
msgstr "@@image: 'easter-eggs-f03.svg'; md5=5a38534f4d800140ecd1b435e43cc30e"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml57(None)
msgid "@@image: 'easter-eggs-f04.svg'; md5=3622b6ec62e44129de17479810628cf9"
msgstr "@@image: 'easter-eggs-f04.svg'; md5=3622b6ec62e44129de17479810628cf9"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml73(None)
msgid "@@image: 'easter-eggs-f05.svg'; md5=6d831b891d409b8ae94729b0e9df84de"
msgstr "@@image: 'easter-eggs-f05.svg'; md5=6d831b891d409b8ae94729b0e9df84de"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml85(None)
msgid "@@image: 'easter-eggs-f06.svg'; md5=bd6db6c45ad35f45f45a4094a89967ef"
msgstr "@@image: 'easter-eggs-f06.svg'; md5=bd6db6c45ad35f45f45a4094a89967ef"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml99(None)
msgid "@@image: 'easter-eggs-f07.svg'; md5=fc5e4dbadf86b35ec5bf261bc78e6c64"
msgstr "@@image: 'easter-eggs-f07.svg'; md5=fc5e4dbadf86b35ec5bf261bc78e6c64"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml110(None)
msgid "@@image: 'easter-eggs-f08.svg'; md5=c4c18c4752462754b72f5aa47122bb64"
msgstr "@@image: 'easter-eggs-f08.svg'; md5=c4c18c4752462754b72f5aa47122bb64"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml121(None)
msgid "@@image: 'easter-eggs-f09.svg'; md5=6c49da108b4969e3e6a4804886189b8f"
msgstr "@@image: 'easter-eggs-f09.svg'; md5=6c49da108b4969e3e6a4804886189b8f"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml133(None)
msgid "@@image: 'easter-eggs-f10.svg'; md5=4f5dd30646531f1515abd1174a854f78"
msgstr "@@image: 'easter-eggs-f10.svg'; md5=4f5dd30646531f1515abd1174a854f78"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml147(None)
msgid "@@image: 'easter-eggs-f11.svg'; md5=f2aa5c31f7f8984ad1ca8ba7b840d36c"
msgstr "@@image: 'easter-eggs-f11.svg'; md5=f2aa5c31f7f8984ad1ca8ba7b840d36c"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml160(None)
msgid "@@image: 'easter-eggs-f12.svg'; md5=777eba2a7a9249dbd0f8f4e9fb3d4bd3"
msgstr "@@image: 'easter-eggs-f12.svg'; md5=777eba2a7a9249dbd0f8f4e9fb3d4bd3"

#. When image changes, this message will be marked fuzzy or untranslated for
#. you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml173(None)
msgid "@@image: 'easter-eggs-f13.svg'; md5=3acd13b727b1626fee9da9d6199f0ebe"
msgstr "@@image: 'easter-eggs-f13.svg'; md5=3acd13b727b1626fee9da9d6199f0ebe"

#: tutorial-easter-eggs.xml4(title)
msgid "Easter Eggs"
msgstr "Húsvéti tojások"

#: tutorial-easter-eggs.xml8(para)
msgid ""
"This tutorial covers the creation of Easter Eggs — basic elliptical shapes "
"that are formed using bezier lines."
msgstr ""
"Ez az ismertető húsvéti tojások készítéséről szól, amelyek ellipszis alakúak"
" és Bézier-vonalakból állnak."

#: tutorial-easter-eggs.xml15(title)
msgid "Creating the Egg"
msgstr "Tojás készítése"

#: tutorial-easter-eggs.xml17(para)
msgid ""
"To make the creation of the egg points easier, turn on the grid and snap. "
"Then create the basic egg shape using the bezier pencil."
msgstr ""
"A legegyszerűbb módon így hozhatjuk létre a tojás pontjait: kapcsoljuk be a "
"rácsot és az illesztést. A Bézier ceruzával hozzuk létre a tojás kezdeti "
"alapját.  "

#: tutorial-easter-eggs.xml29(para)
msgid "Use the node selection tool, and make the nodes smooth."
msgstr ""
"A csomópontok kijelölésére szolgáló eszközzel tegyük ívessé a csomópontokat."

#: tutorial-easter-eggs.xml40(para)
msgid ""
"The egg looks a little too fat. Use the node tool and extend the bottom and "
"top node a bit."
msgstr ""
"Így a tojás kissé tömzsi lesz. A csomópont eszközzel kicsit nyújtsa meg az "
"alsó és a felső csomópontot."

#: tutorial-easter-eggs.xml51(para)
msgid "Color our egg, and fatten the outline using the Fill and Stroke tool."
msgstr ""
"Színezzük be a tojást, majd vastagítsuk meg a körvonalat a kitöltés és "
"körvonal eszközzel."

#: tutorial-easter-eggs.xml65(title)
msgid "Adding stripes to the Egg"
msgstr "Csíkok hozzáadása a tojáshoz"

#: tutorial-easter-eggs.xml67(para)
msgid "Start with the Rectangle tool and draw a rectangle across the egg."
msgstr "Kezdjük a téglalap eszközzel és rajzoljunk egy téglalapot a tojásra."

#: tutorial-easter-eggs.xml78(para)
msgid ""
"Select both objects using the <keycap>Shift</keycap> key while selecting. "
"Perform Path Division on the objects."
msgstr ""
"Jelöljük ki a két objektumot a <keycap>Shift</keycap> gombot lenyomva a "
"kijelölés közben. Alkalmazzuk az útvonalak felosztása műveletet rajtuk."

#: tutorial-easter-eggs.xml90(para)
msgid ""
"Eggs are curved surfaces, so adjust the curve of the band by first inserting"
" a node in the middle of the band. Using the node tool, select nodes on both"
" sides of the band using <keycap>Shift</keycap>, and then use the Insert new"
" nodes into selected segments command."
msgstr ""
"Mivel a tojások felülete domború, igazítsunk a szegély ívén azzal, hogy egy "
"csomópontot szúrunk be a szegélybe. A csomópont eszközzel a "
"<keycap>Shift</keycap>  lenyomása mellett jelöljük ki a szegély mindkét "
"végén lévő csomópontot, majd válasszuk az Új csomópont felvétele a kijelölt "
"szakaszra parancsot."

#: tutorial-easter-eggs.xml104(para)
msgid ""
"Using the node selection tool, make the band angles into smooth curves."
msgstr "A csomópont eszközzel tegyük a szegélyt ívessé."

#: tutorial-easter-eggs.xml115(para)
msgid ""
"Color the band a different color by selecting the band, and using the Fill "
"and Stroke options."
msgstr ""
"Színezzük ki a szegélyt valamilyen más színnel kijelölve a szegélyt, majd "
"utána a kitöltés és körvonal beállításait módosítva."

#: tutorial-easter-eggs.xml126(para)
msgid ""
"Add some designs to the band. A star will look great on our egg. After "
"copying the stars onto the egg, use the node tool to adjust the tilt of the "
"star to match the curve of the egg."
msgstr ""
"Adjunk valami formát a szegélynek. Egy csillag jól nézne ki a tojáson. "
"Miután rámásoltuk a csillagot a tojásra, módosítsuk a csillag dőlését a "
"csomópont eszközzel úgy, hogy a csillag illeszkedjen a tojás domború ívére."

#: tutorial-easter-eggs.xml138(para)
msgid ""
"An Easter egg by itself is great, but having several eggs in a frame would "
"be even better. Also, having an upright egg is nice, but tilting it a little"
" will give it some character. Having several eggs that are identical is "
"nice, but having a little variety is nicer. Lets copy our egg, and adjust "
"some things. Group together all the objects in each egg when finished."
msgstr ""
"Egy húsvéti tojás önmagában is nagyszerű, de több tojás egy keretben még "
"jobb lenne. Egy álló tojás szép, de kicsit megdöntve karakteres lesz. Az "
"egyforma tojások szépek, de a változatosság szebb. Másoljuk le a tojást és "
"alakítsuk át egy kissé. Amikor kész, fogjuk össze az összes tojást egy "
"csoportba."

#: tutorial-easter-eggs.xml152(para)
msgid ""
"After using the handles to adjust the size of the eggs so there is a little "
"variety, use the handles to rotate each egg to its desired position. Raise "
"or lower each egg to get the desired perspective."
msgstr ""
"Miután a vezérlőelemekkel módosítottuk a tojások méretét, hogy legyen némi "
"változatosság, forgassuk el a vezérlőelemekkel a tojásokat a kívánt "
"pozícióba. Módosítsuk a tojások sorrendjét, hogy a kívánt látványt kapjuk."

#: tutorial-easter-eggs.xml165(para)
msgid ""
"Use the rectangle tool to frame our eggs and add a background. Use the Fill "
"and Stroke tool to adjust the border size and the background color. Lower "
"the background rectangle behind the eggs, and adjust the edges to meet the "
"eggs at the bottom and the sides."
msgstr ""
"A téglalap eszközzel keretezzük be a tojásokat és adjunk meg egy háttérszínt"
" is. A kitöltés és körvonal eszközzel módosítsuk a szegély méretét és a "
"háttérszínt. Süllyesszük a háttér négyzetet a tojások mögé, és  módosítsuk a"
" sarkokat úgy, hogy érintsék alul és oldalt a tojásokat."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2.
#: tutorial-easter-eggs.xml0(None)
msgid "translator-credits"
msgstr "Gyuris Gellért <bubu at ujevangelizacio.hu> 2018"
